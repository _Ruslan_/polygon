<?php

declare(strict_types=1);

namespace  Chocofamily\Polygon\Domain\Repositories;

use Chocofamily\Polygon\Infrastructure\Models\Polygon;

interface PolygonRepository
{
    /**
     * @param array $data
     * @return Polygon
     */
    public function store(array $data): Polygon;

    /**
     * @param int $id
     * @param array $data
     * @return Polygon
     */
    public function updateOrCreate(int $id, array $data): Polygon;

    /**
     * @param Polygon $polygon
     * @param array   $data
     * @return Polygon
     */
    public function update(Polygon $polygon, array $data): Polygon;

    /**
     * @param int $id
     * @return Polygon
     */
    public function getActivePolygonById(int $id): Polygon;

    /**
     * @param int $warehouseId
     * @param float $long
     * @param float $lat
     * @return Polygon|null
     */
    public function getPolygonByWithinGeo(int $warehouseId, float $long, float $lat): ?Polygon;

    /**
     * @param int $warehouseId
     * @param float $long
     * @param float $lat
     * @return Polygon
     */
    public function getClosestPolygon(int $warehouseId, float $long, float $lat): Polygon;

    /**
     * @param array $data
     * @return void
     */
    public function upsert(array $data): void;
}
