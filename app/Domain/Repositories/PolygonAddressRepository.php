<?php

declare(strict_types=1);

namespace  Chocofamily\Polygon\Domain\Repositories;

use Chocofamily\Polygon\Infrastructure\Models\PolygonAddress;

interface PolygonAddressRepository
{
    /**
     * @param int $warehouseId
     * @param float $long
     * @param float $lat
     *
     * @return PolygonAddress|null
     */
    public function getActiveAddress(int $warehouseId, float $long, float $lat): ?PolygonAddress;

    /**
     * @param array $data
     * @return PolygonAddress
     */
    public function store(array $data): PolygonAddress;

    /**
     * @param PolygonAddress $polygonAddress
     * @param array $data
     * @return PolygonAddress
     */
    public function update(PolygonAddress $polygonAddress, array $data): PolygonAddress;

    /**
     * @param int $id
     * @param array $data
     * @return PolygonAddress
     */
    public function updateOrCreate(int $id, array $data): PolygonAddress;

    /**
     * @param array $data
     * @return void
     */
    public function upsert(array $data): void;

    /**
     * @param PolygonAddress $polygonAddress
     * @return void
     */
    public function destroy(PolygonAddress $polygonAddress): void;
}
