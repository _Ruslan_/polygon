<?php

namespace Chocofamily\Polygon\Application\Imports;

use Chocofamily\Polygon\Ports\Http\Traits\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class PolygonImport implements ToModel, WithValidation, SkipsOnFailure, WithStartRow
{
    use SkipsFailures;

    private array $data = [];

    /**
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|void|null
     */
    public function model(array $row)
    {
        $geo = \MStaack\LaravelPostgis\Geometries\Polygon::fromString(data_get($row, 1));

        $this->data[] = [
            'id'            => data_get($row, 0),
            'geo_data'      => $geo->toWKT(),
            'geo_wkt'       => $geo->toWKT(),
            'centroid_lat'  => data_get($row, 2),
            'centroid_long' => data_get($row, 3),
            'distance'      => data_get($row, 4),
            'eta'           => data_get($row, 5),
            'warehouse_id'  => data_get($row, 6),
            'is_active'     => is_null(data_get($row, 7)) ? false : data_get($row, 7),
            'properties'    => is_null(data_get($row, 8)) ? null : data_get($row, 8),
            'is_delivery_chargeable' => is_null(data_get($row, 9)) ? null : data_get($row, 9),
        ];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'required|integer',
            'required|string',
            'required|numeric|between:0,90',
            'required|numeric|between:0,90',
            'required|integer|min:1',
            'required|integer|min:1',
            'required|integer|min:1',
            'nullable|boolean',
            'nullable|string',
            'nullable|boolean',
        ];
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
