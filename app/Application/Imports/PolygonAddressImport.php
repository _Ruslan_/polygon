<?php

namespace Chocofamily\Polygon\Application\Imports;

use Chocofamily\Polygon\Ports\Http\Traits\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class PolygonAddressImport implements ToModel, WithValidation, SkipsOnFailure, WithStartRow
{
    use SkipsFailures;

    private array $data = [];

    /**
     * @param array $row
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|void|null
     */
    public function model(array $row)
    {
        $this->data[] = [
            'id'   => data_get($row, 0),
            'lat'  => data_get($row, 1),
            'long' => data_get($row, 2),
            'polygon_id'   => data_get($row, 3),
            'warehouse_id' => data_get($row, 4),
        ];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'required|integer|min:1',
            'required|numeric|between:0,90',
            'required|numeric|between:0,90',
            'required|integer|min:1',
            'required|integer|min:1',
        ];
    }

    public function getData(): array
    {
        return $this->data;
    }
}
