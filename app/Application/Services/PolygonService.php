<?php

namespace Chocofamily\Polygon\Application\Services;

use Chocofamily\Polygon\Domain\Repositories\PolygonAddressRepository;
use Chocofamily\Polygon\Domain\Repositories\PolygonRepository;
use Chocofamily\Polygon\Infrastructure\Models\Polygon;
use Illuminate\Support\Facades\Log;

class PolygonService
{
    /** @var PolygonRepository */
    private $polygonRepository;

    /** @var PolygonAddressRepository */
    private $polygonAddressRepository;

    /**
     * PolygonService constructor.
     */
    public function __construct()
    {
        $this->polygonRepository = app()->make(PolygonRepository::class);
        $this->polygonAddressRepository = app()->make(PolygonAddressRepository::class);
    }

    /**
     * @param int $warehouseId
     * @param float $long
     * @param float $lat
     *
     * @return Polygon
     */
    public function getPolygon(int $warehouseId, float $long, float $lat): Polygon
    {
        $polygonAddress = $this->polygonAddressRepository->getActiveAddress($warehouseId, $long, $lat);
        if ($polygonAddress) {
            return $this->polygonRepository->getActivePolygonById($polygonAddress->polygon_id);
        }

        Log::error("not found polygon address lat = $lat, long = $long, warehouse_id = $warehouseId");
        $polygon = $this->polygonRepository->getPolygonByWithinGeo($warehouseId, $long, $lat);

        if (is_null($polygon)) {
            $polygon = $this->polygonRepository->getClosestPolygon($warehouseId, $long, $lat);
        }

        return $polygon;
    }
}
