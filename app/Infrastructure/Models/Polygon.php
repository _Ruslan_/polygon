<?php

namespace Chocofamily\Polygon\Infrastructure\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

class Polygon extends Model
{
    use PostgisTrait;

    const SRID = 4326;
    const EARTH_RADIUS = 6371000;

    protected $table = 'polygons';

    protected $fillable = [
        'geo_wkt',
        'geo_data',
        'centroid_lat',
        'centroid_long',
        'distance',
        'eta',
        'properties',
        'warehouse_id',
        'is_active',
        'is_delivery_chargeable',
    ];

    protected $casts = [
        'properties' => 'array',
    ];

    protected array $postgisFields = [
        'geo_data',
    ];

    protected array $postgisTypes = [
        'geo_data' => [
            'geomtype' => 'geometry',
            'srid'     => self::SRID,
        ],
    ];

    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Prepare a date for array / JSON serialization.
     * In Laravel 7 date format was changed.
     * @link https://laravel.com/docs/7.x/upgrade#date-serialization
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
