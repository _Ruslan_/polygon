<?php

namespace Chocofamily\Polygon\Infrastructure\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

class PolygonAddress extends Model
{
    protected $table = 'polygon_addresses';

    protected $fillable = [
        'lat',
        'long',
        'warehouse_id',
        'polygon_id',
    ];

    protected $dateFormat = 'Y-m-d H:i:s';

    protected $casts = [
        'lat'  => 'float',
        'long' => 'float',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function polygon()
    {
        return $this->belongsTo(Polygon::class);
    }

    /**
     * Prepare a date for array / JSON serialization.
     * In Laravel 7 date format was changed.
     * @link https://laravel.com/docs/7.x/upgrade#date-serialization
     *
     * @param  \DateTimeInterface  $date
     * @return string
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
