<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Infrastructure;

use Illuminate\Contracts\Auth\Authenticatable;

final class User implements Authenticatable
{
    private int $id;

    /**
     * User constructor.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function getAuthIdentifierName(): string
    {
        return 'id';
    }

    public function getAuthIdentifier(): int
    {
        return $this->id;
    }

    public function getAuthPassword(): string
    {
        return '';
    }

    public function getRememberToken(): string
    {
        return '';
    }

    public function setRememberToken($value): void
    {
        // TODO: Implement setRememberToken() method.
    }

    public function getRememberTokenName(): string
    {
        return '';
    }
}
