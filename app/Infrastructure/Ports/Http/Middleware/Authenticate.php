<?php

namespace Chocofamily\Polygon\Infrastructure\Ports\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as BaseAuthenticate;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class Authenticate
 *
 * @package Application\Http\Middleware
 */
class Authenticate extends BaseAuthenticate
{
    /**
     * Handle an unauthenticated user.
     *
     * @param \Illuminate\Http\Request $request
     * @param array $guards
     *
     * @return void
     *
     */
    protected function unauthenticated($request, array $guards)
    {
        /** @psalm-suppress PossiblyInvalidArgument */
        throw new AccessDeniedHttpException(__('auth.denied'));
    }
}
