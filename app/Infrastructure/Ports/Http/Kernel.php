<?php

namespace Chocofamily\Polygon\Infrastructure\Ports\Http;

use Chocofamily\Polygon\Infrastructure\Ports\Http\Middleware\Authenticate;
use Chocofamily\Polygon\Infrastructure\Ports\Http\Middleware\TrimStrings;
use Chocofamily\Polygon\Infrastructure\Ports\Http\Middleware\TrustProxies;
use Chocofamily\Idempotency\Laravel\Middleware as IdempotencyMiddleware;
use Illuminate\Auth\Middleware\Authorize;
use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull;
use Illuminate\Foundation\Http\Middleware\ValidatePostSize;
use Illuminate\Http\Middleware\SetCacheHeaders;
use Illuminate\Routing\Middleware\SubstituteBindings;
use Illuminate\Routing\Middleware\ValidateSignature;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        TrustProxies::class,
        ValidatePostSize::class,
        TrimStrings::class,
        ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'api' => [
            SubstituteBindings::class,
            IdempotencyMiddleware::class,
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
     //   'auth'          => Authenticate::class,
        'cache.headers' => SetCacheHeaders::class,
        'can'           => Authorize::class,
        'signed'        => ValidateSignature::class
    ];
}
