<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Infrastructure\Repositories;

use Chocofamily\Polygon\Domain\Repositories\PolygonAddressRepository;
use Chocofamily\Polygon\Infrastructure\Exceptions\ServerException;
use Chocofamily\Polygon\Infrastructure\Models\Polygon;
use Chocofamily\Polygon\Infrastructure\Models\PolygonAddress;

class LaravelPolygonAddressRepository implements PolygonAddressRepository
{
    /**
     * @inheritDoc
     */
    public function getActiveAddress(int $warehouseId, float $long, float $lat): ?PolygonAddress
    {
        return PolygonAddress::query()
            ->where('lat', $lat)
            ->where('long', $long)
            ->where('warehouse_id', $warehouseId)
            ->whereHas('polygon', function ($query) {
                $query->where('is_active', true);
            })
            ->first();
    }

    /**
     * @inheritDoc
     */
    public function store(array $data): PolygonAddress
    {
        try {
            $polygon = Polygon::find($data['polygon_id']);
            data_set($data, 'warehouse_id', $polygon->warehouse_id);

            return PolygonAddress::create($data);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }

    /**
     * @inheritDoc
     */
    public function update(PolygonAddress $polygonAddress, array $data): PolygonAddress
    {
        try {
            $polygonAddress->update($data);
            return $polygonAddress;
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }

    /**
     * @inheritDoc
     */
    public function updateOrCreate(int $id, array $data): PolygonAddress
    {
        try {
            $polygonAddress = PolygonAddress::updateOrCreate(
                [
                    'id' => $id
                ],
                $data
            );
            return $polygonAddress;
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }

    /**
     * @inheritDoc
     */
    public function upsert(array $data): void
    {
        try {
            PolygonAddress::query()->upsert($data, 'id', ['lat', 'long', 'polygon_id', 'warehouse_id']);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }

    /**
     * @inheritDoc
     */
    public function destroy(PolygonAddress $polygonAddress): void
    {
        try {
            $polygonAddress->delete();
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }
}
