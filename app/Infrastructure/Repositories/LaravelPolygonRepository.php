<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Infrastructure\Repositories;

use Chocofamily\Polygon\Domain\Repositories\PolygonRepository;
use Chocofamily\Polygon\Infrastructure\Exceptions\AccessDeniedException;
use Chocofamily\Polygon\Infrastructure\Exceptions\NotFoundException;
use Chocofamily\Polygon\Infrastructure\Exceptions\ServerException;
use Chocofamily\Polygon\Infrastructure\Models\Polygon;
use MStaack\LaravelPostgis\Geometries\Point;

class LaravelPolygonRepository implements PolygonRepository
{
    /**
     * @inheritDoc
     */
    public function store(array $data): Polygon
    {
        try {
            $geo = \MStaack\LaravelPostgis\Geometries\Polygon::fromString($data['geo_data']);

            $polygon = new Polygon();
            $polygon->geo_data      = $geo;
            $polygon->geo_wkt       = $geo->toWKT();
            $polygon->centroid_lat  = $data['centroid_lat'];
            $polygon->centroid_long = $data['centroid_long'];
            $polygon->distance      = $data['distance'];
            $polygon->eta           = $data['eta'];
            $polygon->warehouse_id  = $data['warehouse_id'];
            $polygon->is_active              = $data['is_active'];
            $polygon->is_delivery_chargeable = $data['is_delivery_chargeable'];

            $polygon->save();

            return $polygon;
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }

    /**
     * @inheritDoc
     */
    public function updateOrCreate(int $id, array $data): Polygon
    {
        try {
            $polygon = Polygon::updateOrCreate(
                [
                    'id' => $id
                ],
                $data
            );

            return $polygon;
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }

    /**
     * @inheritDoc
     */
    public function update(Polygon $polygon, array $data): Polygon
    {
        try {
            $geo = \MStaack\LaravelPostgis\Geometries\Polygon::fromString($data['geo_data']);
            $polygon->geo_data      = $geo;
            $polygon->geo_wkt       = $geo->toWKT();
            $polygon->centroid_lat  = $data['centroid_lat'];
            $polygon->centroid_long = $data['centroid_long'];
            $polygon->distance      = $data['distance'];
            $polygon->eta           = $data['eta'];
            $polygon->is_active     = $data['is_active'];
            $polygon->is_delivery_chargeable = $data['is_delivery_chargeable'];
            $polygon->save();

            return $polygon;
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }

    /**
     * @inheritDoc
     */
    public function getActivePolygonById(int $id): Polygon
    {
        $polygon = Polygon::find($id);

        if (is_null($polygon)) {
            throw new NotFoundException('полигон не найдено ID ' . $id);
        }
        if ($polygon->is_active == false) {
            throw new AccessDeniedException('полигон не активен ID ' . $id);
        }

        return $polygon;
    }

    /**
     * @inheritDoc
     */
    public function getPolygonByWithinGeo(int $warehouseId, float $long, float $lat): ?Polygon
    {
        $point = new Point($lat, $long);

        return Polygon::query()
            ->where('warehouse_id', $warehouseId)
            ->where('is_active', true)
            ->whereRaw("st_within(st_geomfromtext(?, ?), geo_data)", [$point->toWKT(), Polygon::SRID])
            ->first();
    }

    /**
     * @inheritDoc
     */
    public function getClosestPolygon(int $warehouseId, float $long, float $lat): Polygon
    {
        $p = M_PI / 180;
        $r = 2 * Polygon::EARTH_RADIUS;

        $polygon = Polygon
            ::query()
            ->select(['id', 'distance', 'eta', 'warehouse_id', 'properties', 'is_delivery_chargeable'])
            ->selectRaw(
                '(? * asin(sqrt(0.5 - cos((? - centroid_lat) * ?)/2 + ' .
                'cos(centroid_lat * ?) * cos(?) * (1 - cos((? - centroid_long) * ?)) / 2 ))) as d',
                [$r, $lat, $p, $p, $lat * $p, $long, $p]
            )
            ->where('warehouse_id', $warehouseId)
            ->where('is_active', true)
            ->orderBy('d')
            ->first();

        if (is_null($polygon)) {
            throw new NotFoundException();
        }

        return $polygon;
    }

    /**
     * @inheritDoc
     */
    public function upsert(array $data): void
    {
        try {
            Polygon::query()->upsert($data, 'id', ['geo_data', 'geo_wkt', 'centroid_lat', 'centroid_long', 'distance', 'eta', 'warehouse_id', 'is_active', 'properties', 'is_delivery_chargeable']);
        } catch (\Exception $e) {
            app('sentry')->captureException($e);
            throw new ServerException('системная ошибка');
        }
    }
}
