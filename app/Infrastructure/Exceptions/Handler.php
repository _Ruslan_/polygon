<?php

namespace Chocofamily\Polygon\Infrastructure\Exceptions;

use Chocofamily\Polygon\Ports\Http\Resources\JsonErrorResource;
use CloudCreativity\LaravelJsonApi\Exceptions\HandlesErrors;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Exception\SuspiciousOperationException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    use HandlesErrors;

    protected array $dontReportSentry = [
        \Illuminate\Validation\ValidationException::class,
        \CloudCreativity\LaravelJsonApi\Exceptions\ResourceNotFoundException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException::class,
        \Neomerx\JsonApi\Exceptions\JsonApiException::class,
        \Illuminate\Auth\AuthenticationException::class,
        \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException::class,
        \CloudCreativity\LaravelJsonApi\Exceptions\DocumentRequiredException::class,
        \DomainException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<array-key, string>
     */
    protected $dontFlash = [];

    public function render($request, Throwable $e)
    {
        if ($e instanceof SuspiciousOperationException) {
            $e = new NotFoundHttpException(null, $e);
        }

        if ($this->isJsonApi($request, $e)) {
            $e = $this->prepareException($e);

            return $this->renderJsonApi($request, $e);
        }

        return parent::render($request, $e);
    }

    /**
     * @psalm-suppress MixedMethodCall
     * @psalm-suppress PossiblyUndefinedMethod
     */
    public function report(Throwable $e)
    {
        if ($this->shouldReportSentry($e) && app()->bound('sentry')) {
            app('sentry')->captureException($e);
        }

        parent::report($e);
    }

    protected function shouldReportSentry(Throwable $e): bool
    {
        return null === Arr::first(
            $this->dontReportSentry,
            function (string $type) use ($e) {
                return $e instanceof $type;
            }
        );
    }

    /**
     * @param Request $request
     * @param Throwable $e
     *
     * @psalm-suppress ImplementedReturnTypeMismatch
     * @return JsonErrorResource
     */
    protected function prepareJsonResponse($request, Throwable $e)
    {
        return JsonErrorResource::make($e->getMessage(), null);
    }

    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param Throwable $e
     *
     * @return bool
     */
    protected function isShowException(Throwable $e): bool
    {
        return $e instanceof \DomainException || $e instanceof \InvalidArgumentException;
    }

    /**
     * @param Throwable $e
     *
     * @return int
     */
    protected function getHttpStatusCode(Throwable $e): int
    {
        if ($this->isHttpException($e)) {
            /** @var HttpExceptionInterface $e */
            return $e->getStatusCode();
        }

        if ($this->isShowException($e)) {
            return 400;
        }

        return 500;
    }

    /**
     * Заголовок ошибки
     *
     * @param Throwable $e
     *
     * @return string
     */
    protected function getTitle(Throwable $e): string
    {
        return ($this->isShowException($e) || config('app.debug')) ? $e->getMessage() : 'Сервер недоступен';
    }

    /**
     * Описание ошибки
     *
     * @param Throwable $e
     *
     * @return string
     */
    protected function getDetail(Throwable $e): string
    {
        return ($this->isShowException($e) || config('app.debug'))
            ? $e->getMessage()
            : 'Сервер недоступен, повторите запрос позже';
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
