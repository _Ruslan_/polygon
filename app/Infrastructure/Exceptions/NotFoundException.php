<?php

namespace Chocofamily\Polygon\Infrastructure\Exceptions;

class NotFoundException extends \Exception
{
    protected $code = 404;
}
