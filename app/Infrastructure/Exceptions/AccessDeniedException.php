<?php

namespace Chocofamily\Polygon\Infrastructure\Exceptions;

class AccessDeniedException extends \Exception
{
    protected $code = 403;
}
