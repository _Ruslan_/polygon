<?php

namespace Chocofamily\Polygon\Infrastructure\Exceptions;

class ServerException extends \Exception
{
    protected $code = 500;
}
