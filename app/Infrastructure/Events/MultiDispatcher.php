<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Infrastructure\Events;

use Illuminate\Contracts\Events\Dispatcher;

interface MultiDispatcher extends Dispatcher
{
    /**
     * @param array<array-key, object> $events
     */
    public function multiDispatch(array $events): void;
}
