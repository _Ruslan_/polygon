<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Infrastructure\Events;

use Illuminate\Contracts\Events\Dispatcher;

final class LaravelMultiDispatcher implements MultiDispatcher
{
    private Dispatcher $dispatcher;

    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param array<array-key, object> $events
     */
    public function multiDispatch(array $events): void
    {
        foreach ($events as $event) {
            $this->dispatcher->dispatch($event);
        }
    }

    public function listen($events, $listener = null)
    {
        $this->dispatcher->listen($events, $listener);
    }

    public function hasListeners($eventName)
    {
        return $this->dispatcher->hasListeners($eventName);
    }

    public function subscribe($subscriber)
    {
        $this->dispatcher->subscribe($subscriber);
    }

    public function until($event, $payload = [])
    {
        return $this->dispatcher->until($event, $payload);
    }

    public function dispatch($event, $payload = [], $halt = false)
    {
        return $this->dispatcher->dispatch($event, $payload = [], $halt);
    }

    public function push($event, $payload = [])
    {
        $this->dispatcher->push($event, $payload);
    }

    public function flush($event)
    {
        $this->dispatcher->flush($event);
    }

    public function forget($event)
    {
        $this->dispatcher->forget($event);
    }

    public function forgetPushed()
    {
        $this->dispatcher->forgetPushed();
    }
}
