<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Infrastructure;

use Illuminate\Foundation\Application as BaseApplication;

/** @psalm-suppress all */
class Application extends BaseApplication
{
    protected $appPath = __DIR__ . '/../';

    protected $namespace = "Chocofamily\\Polygon\\";
}
