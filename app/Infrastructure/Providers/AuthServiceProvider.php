<?php

namespace Chocofamily\Polygon\Infrastructure\Providers;

use Chocofamily\Polygon\Infrastructure\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::viaRequest(
            'gateway-header',
            function (Request $request) {
                if ($userId = $request->header('X-User')) {
                    return new User((int) $userId);
                }

                return null;
            }
        );
    }
}
