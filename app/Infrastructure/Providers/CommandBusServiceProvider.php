<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Infrastructure\Providers;

use Illuminate\Support\Facades\Bus;
use Illuminate\Support\ServiceProvider;

class CommandBusServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        Bus::map(
            [
            ]
        );
    }
}
