<?php

namespace Chocofamily\Polygon\Infrastructure\Providers;

use Chocofamily\Polygon\Domain\Repositories\PolygonAddressRepository;
use Chocofamily\Polygon\Domain\Repositories\PolygonRepository;
use Chocofamily\Polygon\Infrastructure\Events\LaravelMultiDispatcher;
use Chocofamily\Polygon\Infrastructure\Events\MultiDispatcher;
use Chocofamily\Polygon\Infrastructure\Repositories\LaravelPolygonAddressRepository;
use Chocofamily\Polygon\Infrastructure\Repositories\LaravelPolygonRepository;
use CloudCreativity\LaravelJsonApi\LaravelJsonApi;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public array $bindings = [
        MultiDispatcher::class => LaravelMultiDispatcher::class,
        PolygonRepository::class => LaravelPolygonRepository::class,
        PolygonAddressRepository::class => LaravelPolygonAddressRepository::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        LaravelJsonApi::defaultApi('v1');
        LaravelJsonApi::showValidatorFailures();
    }
}
