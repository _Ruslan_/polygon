<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Shared\Application\JsonApi\Resolver;

use Chocofamily\Polygon\Shared\Application\JsonApi\Validators\Validators;
use CloudCreativity\LaravelJsonApi\Resolver\AbstractResolver;
use Illuminate\Support\Str;

final class Resolver extends AbstractResolver
{
    /**
     * @param string $unit
     * @param string $resourceType
     *
     * @return string
     */
    protected function resolve($unit, $resourceType)
    {
        $module = ucfirst(Str::singular($resourceType));

        return "Chocofamily\\Contract\\Ports\\Http\\{$unit}\\{$module}";
    }

    /**
     * Кастомный резолвер валидатора. Если Validators не определен для ресурса, возвращает Validators из Shared.
     *
     * @param string $resourceType
     *
     * @return string
     *
     * @psalm-suppress RedundantConditionGivenDocblockType
     */
    public function getValidatorsByResourceType($resourceType)
    {
        /** @psalm-suppress DocblockTypeContradiction */
        return $this->resolve('Validators', $resourceType) ?? Validators::class;
    }
}
