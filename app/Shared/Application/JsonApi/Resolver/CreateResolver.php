<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Shared\Application\JsonApi\Resolver;

final class CreateResolver
{
    public function __invoke(string $apiName, array $config): Resolver
    {
        /** @psalm-suppress MixedArgument */
        return new Resolver($config['resources']);
    }
}
