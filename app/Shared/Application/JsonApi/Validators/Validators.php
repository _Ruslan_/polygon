<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Shared\Application\JsonApi\Validators;

use CloudCreativity\LaravelJsonApi\Validation\AbstractValidators;

final class Validators extends AbstractValidators
{
    /**
     * @param mixed $record
     * @param array $data
     *
     * @return array
     */
    protected function rules($record, array $data): array
    {
        return [];
    }

    protected function queryRules(): array
    {
        return [
            'page.limit' => 'filled|integer|between:1,100',
            'page.page'  => 'filled|integer|min:1',
        ];
    }
}
