<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Shared\Ports\Http;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

/**
 * @OA\Info(
 *      title="Микросервис Polygon",
 *      version="1.0",
 *      description="Микросервис Polygon",
 *      @OA\Contact(
 *          email="zhumabek.r@ryadom.kz"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 */
abstract class Controller extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;
}
