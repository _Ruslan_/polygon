<?php

namespace Chocofamily\Polygon\Shared\Ports\Resources;

/**
 * Class PartnerProposalResource
 *
 * @OA\Schema(
 *      @OA\Property(
 *          property="id",
 *          type="string"
 *      )
 * )
 */
class NewResource extends Resource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
        ];
    }
}
