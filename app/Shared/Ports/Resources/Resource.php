<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Shared\Ports\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Resource
 */
class Resource extends JsonResource
{
}
