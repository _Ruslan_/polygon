<?php

namespace Chocofamily\Polygon\Shared\Ports\Requests;

use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

abstract class FormRequest extends LaravelFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize(): bool;

    /**
     * Handle a failed authorization attempt.
     * @psalm-suppress all
     * @return void
     */
    protected function failedAuthorization()
    {
        throw new AccessDeniedHttpException(__('auth.denied'));
    }
}
