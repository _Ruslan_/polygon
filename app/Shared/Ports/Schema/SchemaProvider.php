<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Shared\Ports\Schema;

use Neomerx\JsonApi\Schema\SchemaProvider as BaseSchemaProvider;

abstract class SchemaProvider extends BaseSchemaProvider
{
}
