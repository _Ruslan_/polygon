<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Ports\Console\Commands;

use Chocofamily\Polygon\Infrastructure\Models\Polygon;
use Illuminate\Console\Command;

class PolygonGeoWktFillCommand extends Command
{
    protected $signature = 'polygon_geo_wkt:fill';

    protected $description = 'Fill new column geo wkt';

    public function handle(): int
    {
        $this->info('START ...');

        $polygons = Polygon::query()
            ->whereNull('geo_wkt')
            ->get()
        ;

        foreach ($polygons as $polygon) {
            /** @var Polygon $polygon */
            $polygon->geo_wkt = $polygon->geo_data->toWKT();
            $polygon->save();
        }

        $this->info('END ...');
        return 0;
    }
}
