<?php

declare(strict_types=1);

namespace Chocofamily\Polygon\Ports\Console\Commands;

use Illuminate\Console\Command;

class ExampleCommand extends Command
{
    protected $signature = 'example:command';

    protected $description = 'Example command';

    public function handle(): int
    {
        $this->info('Example command...');

        return 0;
    }
}
