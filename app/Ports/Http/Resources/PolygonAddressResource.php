<?php

namespace Chocofamily\Polygon\Ports\Http\Resources;

use Chocofamily\Polygon\Shared\Ports\Resources\Resource;

/**
 * Class PolygonAddressResource
 *
 * @OA\Schema(
 *      @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="warehouse_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="polygon_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="lat",
 *          type="number"
 *      ),
 *      @OA\Property(
 *          property="long",
 *          type="number"
 *      ),
 * )
 */
class PolygonAddressResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'lat'          => $this->lat,
            'long'         => $this->long,
            'warehouse_id' => $this->warehouse_id,
            'polygon_id'   => $this->polygon_id,
        ];
    }
}
