<?php

namespace Chocofamily\Polygon\Ports\Http\Resources;

use Chocofamily\Polygon\Shared\Ports\Resources\Resource;

/**
 * Class PolygonResource
 *
 * @OA\Schema(
 *      @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="geo_data",
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="centroid_lat",
 *          type="number"
 *      ),
 *      @OA\Property(
 *          property="centroid_long",
 *          type="number"
 *      ),
 *      @OA\Property(
 *          property="warehouse_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="distance",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="eta",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="is_active",
 *          type="boolean"
 *      ),
 *      @OA\Property(
 *          property="is_delivery_chargeable",
 *          type="boolean"
 *      ),
 *      @OA\Property(
 *          property="properties",
 *          type="array",
 *          @OA\Items()
 *      ),
 * )
 */
class PolygonResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'geo_data'      => $this->geo_data->toWKT() ,
            'centroid_lat'  => $this->centroid_lat,
            'centroid_long' => $this->centroid_long,
            'distance'      => $this->distance,
            'eta'           => $this->eta,
            'warehouse_id'  => $this->warehouse_id,
            'properties'    => json_encode($this->properties),
            'is_delivery_chargeable' => $this->is_delivery_chargeable,
            'is_active'     => $this->is_active,
        ];
    }
}
