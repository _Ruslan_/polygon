<?php

namespace Chocofamily\Polygon\Ports\Http\Resources;

use Chocofamily\Polygon\Shared\Ports\Resources\Resource;

/**
 * Class JsonErrorResource
 *
 * @OA\Schema(
 *      @OA\Property(
 *          property="error_code",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="message",
 *          type="string"
 *      ),
 *      @OA\Property(
 *          property="data",
 *          type="array",
 *          @OA\Items()
 *      ),
 * )
 */
class JsonErrorResource extends Resource
{
    private string $message;

    /**
     * JsonResource constructor.
     * @param string $message
     * @param mixed $resource
     */
    public function __construct(string $message, $resource)
    {
        $this->message = $message;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'error_code' => 1,
            'message'    => $this->message,
            'data'       => null,
        ];
    }
}
