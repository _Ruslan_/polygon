<?php

namespace Chocofamily\Polygon\Ports\Http\Resources;

use Chocofamily\Polygon\Infrastructure\Exceptions\NotFoundException;
use Chocofamily\Polygon\Shared\Ports\Resources\Resource;

/**
 * Class JsonCollection
 *
 * @OA\Schema(
 *      @OA\Property(
 *          property="error_code",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="data",
 *          type="array",
 *          @OA\Items(
 *              @OA\Schema(
 *                  @OA\Property(
 *                      property="pagination",
 *                      type="array",
 *                      @OA\Items (
 *                          @OA\Schema (
 *                              @OA\Property (
 *                                  property="current_page",
 *                                  type="integer",
 *                              )
 *                          )
 *                      ),
 *                      @OA\Items (
 *                          @OA\Schema (
 *                              @OA\Property (
 *                                  property="limit",
 *                                  type="integer",
 *                              )
 *                          )
 *                      ),
 *                      @OA\Items (
 *                          @OA\Schema (
 *                              @OA\Property (
 *                                  property="total_items",
 *                                  type="integer",
 *                              )
 *                          )
 *                      ),
 *                      @OA\Items (
 *                          @OA\Schema (
 *                              @OA\Property (
 *                                  property="total_pages",
 *                                  type="integer",
 *                              )
 *                          )
 *                      ),
 *                  )
 *              ),
 *          )
 *      ),
 * )
 */
class JsonCollection extends Resource
{
    private string $class;
    private bool $withPagination;

    /**
     * JsonCollection constructor.
     * @param string $class
     * @param mixed $resource
     * @param bool $withPagination
     */
    public function __construct(string $class, $resource, bool $withPagination = true)
    {
        $this->class = $class;
        $this->withPagination = $withPagination;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if (!class_exists($this->class)) {
            throw new NotFoundException('resource class not found');
        }

        $data = [
            'items' => $this->class::collection($this)->toArray($request),
        ];
        if ($this->withPagination) {
            $data['pagination'] = [
                "current_page" => $this->currentPage(),
                "limit"        => $this->perPage(),
                "total_items"  => $this->total(),
                "total_pages"  => $this->lastPage(),
            ];
        }

        return [
            'error_code' => 0,
            'data'       => $data,
        ];
    }
}
