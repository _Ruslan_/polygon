<?php

namespace Chocofamily\Polygon\Ports\Http\Resources;

use Chocofamily\Polygon\Shared\Ports\Resources\Resource;

/**
 * Class JsonResource
 *
 * @OA\Schema(
 *      @OA\Property(
 *          property="error_code",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="data",
 *          type="array",
 *          @OA\Items()
 *      ),
 * )
 */
class JsonResource extends Resource
{
    private ?string $class;

    /**
     * JsonResource constructor.
     * @param string|null $class
     * @param mixed $resource
     */
    public function __construct(?string $class, $resource)
    {
        $this->class = $class;
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [];
        if (!is_null($this->class) && class_exists($this->class)) {
            $data = $this->class::make($this)->toArray($request);
        }

        return [
            'error_code' => 0,
            'data'       => $data,
        ];
    }
}
