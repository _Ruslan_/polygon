<?php

namespace Chocofamily\Polygon\Ports\Http\Resources;

use Chocofamily\Polygon\Shared\Ports\Resources\Resource;

/**
 * Class PolygonByAddressResource
 *
 * @OA\Schema(
 *      @OA\Property(
 *          property="id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="warehouse_id",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="distance",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="eta",
 *          type="integer"
 *      ),
 *      @OA\Property(
 *          property="properties",
 *          type="array",
 *          @OA\Items()
 *      ),
 *      @OA\Property(
 *          property="is_delivery_chargeable",
 *          type="boolean"
 *      ),
 * )
 */
class PolygonByAddressResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'distance' => $this->distance,
            'eta' => $this->eta,
            'warehouse_id' => $this->warehouse_id,
            'properties' => $this->properties,
            'is_delivery_chargeable' => $this->is_delivery_chargeable,
        ];
    }
}
