<?php

namespace Chocofamily\Polygon\Ports\Http\Requests\Polygon;

use Chocofamily\Polygon\Shared\Ports\Requests\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'geo_data'      => 'required|string',
            'centroid_lat'  => 'required|numeric|between:0,90',
            'centroid_long' => 'required|numeric|between:0,90',
            'distance'      => 'required|integer|min:1',
            'eta'           => 'required|integer|min:1',
            'is_active'     => 'required|boolean',
            'properties'    => 'nullable|array',
            'is_delivery_chargeable'  => 'required|boolean',
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'warehouse_id'  => 'ID склада',
            'centroid_lat'  => 'широка',
            'centroid_long' => 'долгота',
        ];
    }
}
