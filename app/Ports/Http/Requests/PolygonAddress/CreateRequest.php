<?php

namespace Chocofamily\Polygon\Ports\Http\Requests\PolygonAddress;

use Chocofamily\Polygon\Shared\Ports\Requests\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lat'  => 'required|numeric|between:0,90',
            'long' => 'required|numeric|between:0,90',
            'polygon_id' => 'required|integer|exists:polygons,id',
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'polygon_id' => 'ID полигона',
            'lat'  => 'широка',
            'long' => 'долгота',
        ];
    }
}
