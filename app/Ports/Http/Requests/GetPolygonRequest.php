<?php

namespace Chocofamily\Polygon\Ports\Http\Requests;

use Chocofamily\Polygon\Shared\Ports\Requests\FormRequest;

class GetPolygonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_id' => 'required|integer|min:1',
            'lat'          => 'required|numeric|between:-90,90',
            'long'         => 'required|numeric|between:-90,90',
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributes()
    {
        return [
            'warehouse_id' => 'ID склада',
            'lat'          => 'широка',
            'long'         => 'долгота',
        ];
    }
}
