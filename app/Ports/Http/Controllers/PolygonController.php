<?php

namespace Chocofamily\Polygon\Ports\Http\Controllers;

use Chocofamily\Polygon\Application\Imports\PolygonImport;
use Chocofamily\Polygon\Domain\Repositories\PolygonRepository;
use Chocofamily\Polygon\Infrastructure\Models\Polygon;
use Chocofamily\Polygon\Ports\Http\Requests\Polygon\CreateRequest;
use Chocofamily\Polygon\Ports\Http\Requests\Polygon\UpdateRequest;
use Chocofamily\Polygon\Ports\Http\Resources\JsonCollection;
use Chocofamily\Polygon\Ports\Http\Resources\JsonErrorResource;
use Chocofamily\Polygon\Ports\Http\Resources\JsonResource;
use Chocofamily\Polygon\Ports\Http\Resources\PolygonResource;
use Chocofamily\Polygon\Shared\Ports\Http\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PolygonController
 */
class PolygonController extends Controller
{
    /**
     * @OA\Get(
     *      path="/v1/polygon",
     *      operationId="polygon-list",
     *      tags={"Polygon"},
     *      summary="Получить список полигонов",
     *      description="Получить список полигонов",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonResource")
     *          )
     *       ),
     *     )
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonCollection
     */
    public function index(): JsonCollection
    {
        $polygons = QueryBuilder::for(Polygon::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('warehouse_id'),
                AllowedFilter::exact('is_active'),
                AllowedFilter::exact('is_delivery_chargeable'),
            ])
            ->allowedSorts(['id'])
            ->defaultSort('id')
        ;

        return new JsonCollection(PolygonResource::class, $polygons->paginate(10));
    }

    /**
     * @OA\Post(
     *      path="/v1/polygon",
     *      operationId="polygon-store",
     *      tags={"Polygon"},
     *      summary="Создать полигон",
     *      description="Создать полигон",
     *      @OA\Parameter(
     *          name="geo_data",
     *          description="точки полигона",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="centroid_lat",
     *          description="точка центра полигона (широта)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="centroid_long",
     *          description="точка центра полигона (долгота)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="distance",
     *          description="расстояние от склада до полигона в метрах",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="eta",
     *          description="время для пеших от склада до полигона в минутах",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="warehouse_id",
     *          description="ID склада",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_active",
     *          description="Активный ли полигон или нет",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="properties",
     *          description="доп. характиристики",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items()
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_delivery_chargeable",
     *          description="Доставка платная или бесплатная",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonResource")
     *          )
     *       ),
     *     )
     * @param PolygonRepository $repository
     * @param CreateRequest $request
     * @return JsonResource
     */
    public function store(PolygonRepository $repository, CreateRequest $request): JsonResource
    {
        $repository->store($request->validated());
        return JsonResource::make(null, []);
    }

    /**
     * @OA\Get(
     *      path="/v1/polygon/{id}",
     *      operationId="polygon-info",
     *      tags={"Polygon"},
     *      summary="Получить полигон",
     *      description="Получить полигон",
     *     @OA\Parameter(
     *          name="id",
     *          description="ID полигона",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonResource")
     *          )
     *       ),
     *     )
     * @param Polygon $polygon
     * @return JsonResource
     */
    public function show(Polygon $polygon): JsonResource
    {
        return new JsonResource(PolygonResource::class, $polygon);
    }

    /**
     * @OA\Put(
     *      path="/v1/polygon/{id}",
     *      operationId="polygon-update",
     *      tags={"Polygon"},
     *      summary="Обновить полигон",
     *      description="Обновить полигон",
     *      @OA\Parameter(
     *          name="id",
     *          description="ID полигона",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="geo_data",
     *          description="точки полигона",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="centroid_lat",
     *          description="точка центра полигона (широта)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="centroid_long",
     *          description="точка центра полигона (долгота)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="distance",
     *          description="расстояние от склада до полигона в метрах",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="eta",
     *          description="время для пеших от склада до полигона в минутах",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_active",
     *          description="Активный ли полигон или нет",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="properties",
     *          description="доп. характиристики",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items()
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="is_delivery_chargeable",
     *          description="Доставка платная или бесплатная",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="boolean"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonResource")
     *          )
     *       ),
     *     )
     * @param PolygonRepository $repository
     * @param Polygon $polygon
     * @param UpdateRequest $request
     * @return JsonResource
     */
    public function update(PolygonRepository $repository, Polygon $polygon, UpdateRequest $request): JsonResource
    {
        $repository->update($polygon, $request->validated());
        return JsonResource::make(null, []);
    }

    /**
     * @OA\Get(
     *      path="/v1/polygon/export-data",
     *      operationId="polygon-list-for-export",
     *      tags={"Polygon"},
     *      summary="Получить список полигонов",
     *      description="Получить список полигонов",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonResource")
     *          )
     *       ),
     *     )
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonCollection
     */
    public function getExportData(): JsonCollection
    {
        $polygons = QueryBuilder::for(Polygon::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('warehouse_id'),
                AllowedFilter::exact('is_active'),
                AllowedFilter::exact('is_delivery_chargeable'),
            ])
            ->allowedSorts(['id'])
            ->defaultSort('id')
        ;

        return new JsonCollection(PolygonResource::class, $polygons->get(), false);
    }

    /**
     * @OA\Post(
     *      path="/v1/polygon/import",
     *      operationId="polygon-import",
     *      tags={"Polygon"},
     *      summary="Обновить/cоздать полигоны с файла",
     *      description="Обновить/cоздать полигоны с файла",
     *      @OA\Parameter(
     *          name="file",
     *          description="excel файл",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="object"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/JsonResource")
     *          )
     *       )
     * )
     *
     * @param PolygonRepository $repository
     * @param Request $request
     * @return JsonErrorResource|JsonResource
     */
    public function import(PolygonRepository $repository, Request $request)
    {
        set_time_limit(3000);
        ini_set("memory_limit", "256M");
        $import = new PolygonImport();
        $file = $request->file('file');
        if (is_null($file)) {
            return JsonErrorResource::make('file not exists', []);
        }
        if (is_array($file)) {
            $file = current($file);
        }
        Excel::import($import, $file);
        $importData = array_chunk($import->getData(), 300);

        foreach ($importData as $data) {
            $repository->upsert($data);
        }

        if (count($import->failures())) {
            return JsonErrorResource::make($import->getFailMessages(), []);
        }

        return JsonResource::make(null, []);
    }
}
