<?php

namespace Chocofamily\Polygon\Ports\Http\Controllers;

use CloudCreativity\LaravelJsonApi\Http\Controllers\JsonApiController;
use Illuminate\Support\Str;
use Neomerx\JsonApi\Document\Error;

/**
 * Class ErrorController
 *
 * Контроллер для ошибок
 */
final class ErrorController extends JsonApiController
{
    private const CODE    = 404;
    private const MESSAGE = 'Ресурс не найден';

    /**
     * Ответ, если не найден ресурс
     *
     * @return mixed
     */
    public function notFound()
    {
        return $this->reply()->error(
            new Error(
                Str::uuid()->toString(),
                null,
                static::CODE,
                static::CODE,
                static::MESSAGE,
                static::MESSAGE
            )
        );
    }
}
