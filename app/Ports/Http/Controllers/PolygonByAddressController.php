<?php

namespace Chocofamily\Polygon\Ports\Http\Controllers;

use Chocofamily\Polygon\Application\Services\PolygonService;
use Chocofamily\Polygon\Ports\Http\Resources\JsonResource;
use Chocofamily\Polygon\Ports\Http\Resources\PolygonByAddressResource;
use Chocofamily\Polygon\Shared\Ports\Http\Controller;
use Illuminate\Http\Request;

/**
 * Class PolygonController
 *
 * Контроллер для ошибок
 */
class PolygonByAddressController extends Controller
{
    /**
     * @OA\Post(
     *      path="/v1/get-polygon",
     *      operationId="getPolygon",
     *      tags={"Polygon"},
     *      summary="Получить информацию о полигоне",
     *      description="Найти по адресу или по вхождению в границы полигона или наиболее близкую точку от центра по точке доставки",
     *      @OA\Parameter(
     *          name="warehouse_id",
     *          description="ID склада",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="lat",
     *          description="широта",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="long",
     *          description="долгота",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonByAddressResource")
     *          )
     *     ),
     * )
     *
     * @param Request $request
     * @return JsonResource
     */
    public function getPolygon(Request $request)
    {
        $polygon = (new PolygonService())
            ->getPolygon($request->get('warehouse_id'), $request->get('long'), $request->get('lat'));

        return JsonResource::make(PolygonByAddressResource::class, $polygon);
    }
}
