<?php

namespace Chocofamily\Polygon\Ports\Http\Controllers;

use Chocofamily\Polygon\Application\Imports\PolygonAddressImport;
use Chocofamily\Polygon\Domain\Repositories\PolygonAddressRepository;
use Chocofamily\Polygon\Infrastructure\Models\PolygonAddress;
use Chocofamily\Polygon\Ports\Http\Requests\PolygonAddress\CreateRequest;
use Chocofamily\Polygon\Ports\Http\Requests\PolygonAddress\UpdateRequest;
use Chocofamily\Polygon\Ports\Http\Resources\JsonCollection;
use Chocofamily\Polygon\Ports\Http\Resources\JsonErrorResource;
use Chocofamily\Polygon\Ports\Http\Resources\JsonResource;
use Chocofamily\Polygon\Ports\Http\Resources\PolygonAddressResource;
use Chocofamily\Polygon\Shared\Ports\Http\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PolygonAddressController
 */
class PolygonAddressController extends Controller
{
    /**
     * @OA\Get(
     *      path="/v1/polygon-address",
     *      operationId="polygon-address-list",
     *      tags={"Polygon"},
     *      summary="Получить список адресов полигона",
     *      description="Получить список адресов полигона",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonAddressResource")
     *          )
     *       ),
     *     )
     *
     * @param Request $request
     * @return JsonCollection
     */
    public function index(): JsonCollection
    {
        $polygons = QueryBuilder::for(PolygonAddress::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('warehouse_id'),
                AllowedFilter::exact('polygon_id'),
            ])
            ->allowedSorts(['id'])
            ->defaultSort('id')
        ;

        return new JsonCollection(PolygonAddressResource::class, $polygons->paginate(10));
    }

    /**
     * @OA\Post(
     *      path="/v1/polygon-address",
     *      operationId="polygon-address-store",
     *      tags={"Polygon"},
     *      summary="Создать адрес полигона",
     *      description="Создать адрес полигона",
     *      @OA\Parameter(
     *          name="lat",
     *          description="широта",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="long",
     *          description="долгота",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="polygon_id",
     *          description="ID полигона",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/JsonResource")
     *          )
     *       ),
     *     )
     * @param PolygonAddressRepository $repository
     * @param CreateRequest $request
     * @return JsonResource
     */
    public function store(PolygonAddressRepository $repository, CreateRequest $request): JsonResource
    {
        $repository->store($request->validated());
        return JsonResource::make(null, []);
    }

    /**
     * @OA\Get(
     *      path="/v1/polygon-address/{id}",
     *      operationId="polygon-address-info",
     *      tags={"Polygon"},
     *      summary="Получить адрес полигона",
     *      description="Получить адрес полигона",
     *      @OA\Parameter(
     *          name="id",
     *          description="ID адреса",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonAddressResource")
     *          )
     *       ),
     *     )
     * @param PolygonAddress $polygonAddress
     * @return JsonResource
     */
    public function show(PolygonAddress $polygonAddress): JsonResource
    {
        return new JsonResource(PolygonAddressResource::class, $polygonAddress);
    }

    /**
     * @OA\Put(
     *      path="/v1/polygon-address/{id}",
     *      operationId="polygon-address-update",
     *      tags={"Polygon"},
     *      summary="Обновить адрес полигона",
     *      description="Обновить адрес полигона",
     *      @OA\Parameter(
     *          name="id",
     *          description="ID адрес полигона",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="lat",
     *          description="широта",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="long",
     *          description="долгота",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/JsonResource")
     *          )
     *       ),
     *     )
     * @param PolygonAddressRepository $repository
     * @param PolygonAddress $polygonAddress
     * @param UpdateRequest $request
     * @return JsonResource
     */
    public function update(PolygonAddressRepository $repository, PolygonAddress $polygonAddress, UpdateRequest $request)
    {
        $repository->update($polygonAddress, $request->validated());
        return JsonResource::make(null, []);
    }

    /**
     * @OA\Delete(
     *      path="/v1/polygon-address/{id}",
     *      operationId="polygon-address-destroy",
     *      tags={"Polygon"},
     *      summary="Удалить адрес полигона",
     *      description="Удалить адрес полигона",
     *      @OA\Parameter(
     *          name="id",
     *          description="ID адрес полигона",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/JsonResource")
     *          )
     *       ),
     *     )
     * @param PolygonAddressRepository $repository
     * @param PolygonAddress $polygonAddress
     * @return JsonResource
     */
    public function destroy(PolygonAddressRepository $repository, PolygonAddress $polygonAddress)
    {
        $repository->destroy($polygonAddress);
        return JsonResource::make(null, []);
    }

    /**
     * @OA\Get(
     *      path="/v1/polygon-address/export-data",
     *      operationId="polygon-address-list-for-export",
     *      tags={"Polygon"},
     *      summary="Получить список адресов полигона",
     *      description="Получить список адресов полигона",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/PolygonResource")
     *          )
     *       ),
     *     )
     *
     * @param Request $request
     * @return JsonCollection
     */
    public function getExportData()
    {
        $polygonAddresses = QueryBuilder::for(PolygonAddress::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::exact('warehouse_id'),
                AllowedFilter::exact('polygon_id'),
            ])
            ->allowedSorts(['id'])
            ->defaultSort('id')
        ;

        return new JsonCollection(PolygonAddressResource::class, $polygonAddresses->get(), false);
    }

    /**
     * @OA\Post(
     *      path="/v1/polygon-address/import",
     *      operationId="polygon-address-import",
     *      tags={"Polygon"},
     *      summary="Обновить/cоздать адреса полигона с файла",
     *      description="Обновить/cоздать адреса полигона с файла",
     *      @OA\Parameter(
     *          name="file",
     *          description="excel файл",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="object"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\Schema(
     *              type="array",
     *              @OA\Items(ref="#/components/schemas/JsonResource")
     *          )
     *       ),
     * )
     *
     * @param PolygonAddressRepository $repository
     * @param Request $request
     * @return JsonErrorResource|JsonResource
     */
    public function import(PolygonAddressRepository $repository, Request $request)
    {
        set_time_limit(3000);
        ini_set("memory_limit", "256M");
        $import = new PolygonAddressImport();
        $file = $request->file('file');
        if (is_null($file)) {
            return JsonErrorResource::make('file not exists', []);
        }
        if (is_array($file)) {
            $file = current($file);
        }

        Excel::import($import, $file);
        $importData = array_chunk($import->getData(), 500);

        foreach ($importData as $data) {
            $repository->upsert($data);
        }

        if (count($import->failures())) {
            return JsonErrorResource::make($import->getFailMessages(), []);
        }

        return JsonResource::make(null, []);
    }
}
