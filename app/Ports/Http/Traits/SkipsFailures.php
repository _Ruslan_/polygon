<?php

namespace Chocofamily\Polygon\Ports\Http\Traits;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Validators\Failure;

trait SkipsFailures
{
    use \Maatwebsite\Excel\Concerns\SkipsFailures;

    /**
     * @var Failure[]
     */
    protected array $failuresByID = [];

    /**
     * @param int $id
     * @param string $attribute
     * @param array $errors
     * @param array $data
     * @return void
     */
    protected function createFailure(int $id, string $attribute, array $errors, array $data): void
    {
        $fail = new Failure($id, $attribute, $errors, $data);
        array_push($this->failuresByID, $fail);
    }

    /**
     * @return string
     */
    public function getFailMessages(): string
    {
        $msg = '';
        foreach ($this->failures() as $fail) {
            $msg .= 'Строка ' . $fail->row() . '. Ошибка: ' . current($fail->errors());
        }

        foreach ($this->failuresByID() as $fail) {
            $msg .= 'ID ' . $fail->row() . '. Ошибка: ' . current($fail->errors());
        }
        return $msg;
    }

    /**
     * @return Collection
     */
    public function failuresByID(): Collection
    {
        return new Collection($this->failuresByID);
    }
}
