<?php

declare(strict_types=1);

namespace Tests;

abstract class ApiTestCase extends TestCase
{
    /**
     * Шаблон ответа для ресурса
     *
     * @param array $data
     *
     * @return array
     */
    public function resourceStructure(array $data): array
    {
        $template = [
            "data" => [
                'type',
                "id",
                "attributes"    => [],
                "links"         => [],
            ],
        ];

        $template['data'] = array_merge($template['data'], $data);

        return $template;
    }

    /**
     * Шаблон ответа для коллекции
     *
     * @param array $data
     *
     * @param array $include
     *
     * @return array
     */
    public function collectionStructure(array $data, array $include = []): array
    {
        $template = [
            "data"  => [],
            "links" => [
                "first",
                "last",
            ],
            "meta"  => [
                "current_page",
                "per_page",
                "from",
                "to",
                "total",
                "last_page",
            ],
        ];

        $template['data'] = array_merge($template['data'], $data);

        if ($include) {
            $template['include'] = array_merge($template['include'], $include);
        }

        return $template;
    }
}
