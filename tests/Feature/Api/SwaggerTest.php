<?php

namespace Tests\Feature\Api;

use Tests\ApiTestCase;

class SwaggerTest extends ApiTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest(): void
    {
        $response = $this->get('/api/doc');

        $response->assertStatus(200);
    }
}
