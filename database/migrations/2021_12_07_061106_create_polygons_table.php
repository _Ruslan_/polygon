<?php

use Chocofamily\Polygon\Infrastructure\Models\Polygon;
use Illuminate\Database\Migrations\Migration;
use MStaack\LaravelPostgis\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePolygonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polygons', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('warehouse_id');
            $table->decimal('centroid_lat', 20, 15, true);
            $table->decimal('centroid_long', 20, 15, true);
            $table->polygon('geo_data', 'GEOMETRY', Polygon::SRID);
            $table->unsignedInteger('distance');
            $table->unsignedInteger('eta');
            $table->jsonb('properties')->nullable();
            $table->boolean('is_active')->default(1);

            $table->timestamps();

            $table->index(['warehouse_id', 'is_active', 'centroid_lat', 'centroid_long']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('polygons');
    }
}
