<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ChangeIsDeliveryChargeablePolygonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $ids = '1551, 1375, 1410, 1413, 1427, 1435, 1441, 1446, 1464, 1490, 209, 285, 1573, 1574, 115, 1593, 109, 6, 30, 36, 41, 48, 52, 332, 478, 96, 100, 338, 137, 152, 169, 171, 378, 207, 237, 244, 262, 55, 289, 62, 324, 405, 416, 571, 533, 451, 453, 548, 471, 626, 637, 648, 649, 529, 656, 664, 566, 812, 584, 822, 624, 855, 884, 714, 715, 745, 770, 786, 798, 912, 870, 872, 953, 983, 984, 992, 1008, 1025, 1060, 1072, 1068, 1078, 1081, 1089, 1125, 1146, 1175, 1198, 1232, 1238, 1244, 1245, 1168, 1258, 1288, 1311, 1331, 1340, 1356, 1507';

        DB::table('polygons')
            ->whereIn('id', array_map('intval', explode(',', $ids)))
            ->update([
                'is_delivery_chargeable' => true
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $ids = '1551, 1375, 1410, 1413, 1427, 1435, 1441, 1446, 1464, 1490, 209, 285, 1573, 1574, 115, 1593, 109, 6, 30, 36, 41, 48, 52, 332, 478, 96, 100, 338, 137, 152, 169, 171, 378, 207, 237, 244, 262, 55, 289, 62, 324, 405, 416, 571, 533, 451, 453, 548, 471, 626, 637, 648, 649, 529, 656, 664, 566, 812, 584, 822, 624, 855, 884, 714, 715, 745, 770, 786, 798, 912, 870, 872, 953, 983, 984, 992, 1008, 1025, 1060, 1072, 1068, 1078, 1081, 1089, 1125, 1146, 1175, 1198, 1232, 1238, 1244, 1245, 1168, 1258, 1288, 1311, 1331, 1340, 1356, 1507';
        DB::table('polygons')
            ->whereIn('id', array_map('intval', explode(',', $ids)))
            ->update([
                'is_delivery_chargeable' => false
            ]);
    }
}

