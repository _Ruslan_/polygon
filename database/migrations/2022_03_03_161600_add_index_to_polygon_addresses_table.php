<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use MStaack\LaravelPostgis\Schema\Blueprint;

class AddIndexToPolygonAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('polygon_addresses', function (Blueprint $table) {
            $table->index(['warehouse_id', 'lat', 'long'], 'warehouse_id_lat_long_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('polygon_addresses', function (Blueprint $table) {
            $table->dropIndex('warehouse_id_lat_long_index');
        });
    }
}
