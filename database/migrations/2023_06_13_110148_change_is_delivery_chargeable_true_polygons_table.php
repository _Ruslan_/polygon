<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class ChangeIsDeliveryChargeableTruePolygonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('polygons')
            ->where('is_active', true)
            ->where('is_delivery_chargeable', false)
            ->update([
                'is_delivery_chargeable' => true
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('polygons')
            ->where('is_active', true)
            ->where('is_delivery_chargeable', true)
            ->update([
                'is_delivery_chargeable' => false
            ]);
    }
}
