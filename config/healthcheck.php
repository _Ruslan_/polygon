<?php

return [
    'componentChecks' => [
        'DB' => Chocofamilyme\LaravelHealthCheck\Services\Checks\DatabaseComponentCheck::class,
        'CACHE' => Chocofamilyme\LaravelHealthCheck\Services\Checks\CacheComponentCheck::class,
        //'SESSIONS' => Chocofamilyme\LaravelHealthCheck\Services\Checks\SessionsComponentCheck::class,
        'STORAGE' => Chocofamilyme\LaravelHealthCheck\Services\Checks\StorageComponentCheck::class,
    ],
];
