## JSON API
API построен по стандарту [jsonapi v.1](https://jsonapi.org/).
С использованием библиотеки [laravel-json-ap](https://laravel-json-api.readthedocs.io/en/latest/).

Файлы описания API (Adapter, Schema, Validation) помещаются в папку API каждого компонента.

По-умолчанию, если в компоненте нет файла описания Validation, используется общий валидатор Common\Application\JsonApi\Validators\Validators.
