<?php

use Chocofamily\Polygon\Ports\Http\Controllers\ErrorController;
use Chocofamily\Polygon\Ports\Http\Controllers\PolygonAddressController;
use Chocofamily\Polygon\Ports\Http\Controllers\PolygonByAddressController;
use Chocofamily\Polygon\Ports\Http\Controllers\PolygonController;
use Illuminate\Support\Facades\Route;

Route::fallback([ErrorController::class, 'notFound'])
    ->name('api.fallback.404');

Route::prefix('polygon/v1')
    ->group(function () {
        Route::post('get-polygon', [PolygonByAddressController::class, 'getPolygon'])->name('polygon.get_by_address');
        Route::get('polygon/get-export-data', [PolygonController::class, 'getExportData'])->name('polygon.get_export_data');
        Route::post('polygon/import', [PolygonController::class, 'import'])->name('polygon.import');

        Route::get('polygon-address/get-export-data', [PolygonAddressController::class, 'getExportData'])->name('polygon_address.get_export_data');
        Route::post('polygon-address/import', [PolygonAddressController::class, 'import'])->name('polygon_address.import');

        Route::resource('polygon', PolygonController::class)->except('destroy');
        Route::resource('polygon-address', PolygonAddressController::class);
    });
