<?php

require __DIR__.'/vendor/autoload.php';

use hollodotme\FastCGI\Client;
use hollodotme\FastCGI\Requests\GetRequest;
use hollodotme\FastCGI\SocketConnections\NetworkSocket;

const HOST = '127.0.0.1';
const PORT = 9000;
const SCRIPT_FILENAME = '/srv/www/app/public/index.php';
const HEALTH_ENDPOINT = '/health';
const SUCCESS = 0;
const ERROR = 1;

try {
    $client = new Client();

    $connection = new NetworkSocket(HOST, PORT);
    $content = http_build_query([]);
    $request = new GetRequest(SCRIPT_FILENAME, $content);
    $request->setRequestUri(HEALTH_ENDPOINT);

    $response = $client->sendRequest($connection, $request);
    if (array_key_exists('Status', $response->getHeaders())) {
        $status = $response->getHeaders()['Status'][0];
        if (strpos($status, '200') === false) {
            exit(ERROR);
        }
    }

    $body = json_decode($response->getBody(), true);
    if (is_null($body)) {
        exit(ERROR);
    }

    if ($body['error_code'] > 0) {
        exit(ERROR);
    }

    exit(SUCCESS);
} catch (Throwable $e) {
    error_log($e->getMessage());
    exit(ERROR);
}
