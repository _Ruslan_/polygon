# Larabase - шаблон для микросервисов на PHP фреймворке Laravel

## Требуется
- PHP >= 7.4

## Описание
Шаблон для создания микросервисов на основе фреймворка [Laravel](https://laravel.com/).
Оставлен код необходимый для написания приложений обрабатывающих запросы по HTTP в формате REST API.
Т.е нет состояний - сессий, нет вьюшек.

## Структура папок
```
├── Application
│   ├── Command
│   │   └── Example
│── Domain
│   └── Entities
│       └── Example
│── Infrastructure
│   │── Exceptions
│   └── Providers
│── Ports
│   ├── Cli
│   └── Api
|      ├── Adapter
|      ├── Controllers
|      ├── Docs
|      ├── Requests
|      ├── Schema
|      └── Validator
└── Shared (Общие классы)
    ├── Application
    ├── Infrastructure
    └── Infrastructure
```

Необязательно использовать все слои из этого шаблона. Если у вас нет предметной области или она бедная,
оставьте только Infrastructure, Port и Application уровни.

## Запуск сервиса
Создать .env файл на основе .env.example (TODO уточнить, возможно не нужно). Собираем докер образ из исходников

``` docker build -t larabase -f docker/Dockerfile . ```

Запустить, прокинув в контейнер папку с исходниками:
 
``` docker run -v $(pwd):/srv/www/app larabase ```

Не забудь скачать пакеты:

``` docker run -v $(pwd):/srv/www/app larabase composer install ```

## Документация
* Документация генерируется на основе Open APIv3. 
* Swagger файл лежит по умолчанию в папке storage/api-docs/api-docs.json.
* Документация в json доступно по адресу /api/doc
* Документация в UI доступно по адресу /api/swaggerui, см конфиг l5-swagger.php
* После добавление нового маршрута в api.php, нужно запустить команду
```php
php artisan l5-swagger:generate
```

### Используемые библиотеки
- [JSON API](docs/Json-api.md)

### Линтеры

Для автоматический проверки и исправления качество кода используются следующие линтеры

#### [Psalm](https://psalm.dev/)

Конфигурационный файл - [psalm.xml](psalm.xml)
   
Команда для проверки

```sh
vendor/bin/psalm
```

Команда для исправления (фиксит не все ошибки). Подробнее [тут](https://psalm.dev/docs/manipulating_code/fixing/)

```sh
vendor/bin/psalter --issues=issue-name
```   
   
#### [PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) 

Конфигурационный файл - [.php_cs](.php_cs)
   
Команда для проверки

```sh
vendor/bin/php-cs-fixer fix --dry-run
```

Команда для исправления

```sh
vendor/bin/php-cs-fixer fix
```   

## Создание сервиса на основе Larabase
Делаем Fork проекта. После того как сделали форк, отвязываем новый проект от Larabase. Иначе изменения в новом проекте 
будут попадать в Larabase.

Смотрим .env.example, прописываем нужные переменные окружения. Например APP_NAME и APP_URL_BASE_PATH, 
у каждого сервиса должен быть уникальным.

### Правильные namespace в классах сервиса
Для каждого сервиса должен быть свой уникальный namespace. Т.е в классах namespace начинается
```php
<?php

...

namespace Chocofamily\Larabase\***;
...
```

Вместо Chocofamily\Larabase, должно быть имя проекта и название сервиса.
Для этого нужно:
- Поправить в composer.json секцию autoload
- Пробежаться по все классам и сменить namespace на свой.
- Как автоматизировать это не знаю, но надо что-то придумать! Есть вариант с - https://github.com/humbug/php-scoper

#### Настройки для kubernetes
В папке .helm находятся настройки для kubernetes.
values.yaml - файл с общими настройкаи и для staiging и для боя.

prerelease.yaml - файл с настройками для staiging. Перезаписывает одинаковые настройки с values.yaml.

master.yaml - файл с настройками для боя. Перезаписывает одинаковые настройки с values.yaml.
